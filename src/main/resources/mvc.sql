﻿# MySQL-Front 5.0  (Build 1.0)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: localhost    Database: test
# ------------------------------------------------------
# Server version 5.6.10

#
# Table structure for table blog
#

DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户博客';
INSERT INTO `blog` VALUES (1,3,'星星之火','可以燎原');
INSERT INTO `blog` VALUES (2,3,'剑桥国际英语','英语学习法则');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table shiro_role
#

DROP TABLE IF EXISTS `shiro_role`;
CREATE TABLE `shiro_role` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '' COMMENT '角色名称',
  `Description` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户角色';
INSERT INTO `shiro_role` VALUES (1,'admin','管理员');
INSERT INTO `shiro_role` VALUES (3,'user','普通用户');
/*!40000 ALTER TABLE `shiro_role` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table shiro_role_permission
#

DROP TABLE IF EXISTS `shiro_role_permission`;
CREATE TABLE `shiro_role_permission` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `permission` varchar(255) NOT NULL DEFAULT '' COMMENT '权限',
  PRIMARY KEY (`permission`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限';
INSERT INTO `shiro_role_permission` VALUES (1,'user:create');
INSERT INTO `shiro_role_permission` VALUES (1,'user:delete');
INSERT INTO `shiro_role_permission` VALUES (1,'user:update');
/*!40000 ALTER TABLE `shiro_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table shiro_user
#

DROP TABLE IF EXISTS `shiro_user`;
CREATE TABLE `shiro_user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Age` int(11) DEFAULT NULL,
  `LoginName` varchar(50) DEFAULT '' COMMENT '登录名',
  `Password` varchar(255) DEFAULT NULL COMMENT '密码',
  `Salt` varchar(64) DEFAULT NULL,
  `CreateAt` datetime DEFAULT NULL COMMENT '注册日期',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `LoginName唯一约束` (`LoginName`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO `shiro_user` VALUES (1,'仝玉甫',28,'admin',NULL,NULL,NULL);
INSERT INTO `shiro_user` VALUES (3,'仝玉甫',NULL,'tongyufu','22c7a47a84f5e1578321d895e22559ebfa03a699','183aef8f043d8152','2013-01-13 01:43:53');
INSERT INTO `shiro_user` VALUES (5,'仝玉甫',NULL,'tongyufu1','184518b2111119681c749c9f9ddc602bd926c191','21ad3a403dfc5dc7','2013-01-13 18:37:25');
INSERT INTO `shiro_user` VALUES (6,'仝玉甫',NULL,'tongyufu2','a60fd17cf425c85fc94c90cc18fa012ac48bc99e','afe8387dba20accd','2013-01-13 23:19:14');
/*!40000 ALTER TABLE `shiro_user` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table shiro_user_role
#

DROP TABLE IF EXISTS `shiro_user_role`;
CREATE TABLE `shiro_user_role` (
  `User_id` int(11) NOT NULL DEFAULT '0',
  `Role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`User_id`,`Role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色';
INSERT INTO `shiro_user_role` VALUES (3,1);
INSERT INTO `shiro_user_role` VALUES (5,2);
INSERT INTO `shiro_user_role` VALUES (6,1);
/*!40000 ALTER TABLE `shiro_user_role` ENABLE KEYS */;
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
