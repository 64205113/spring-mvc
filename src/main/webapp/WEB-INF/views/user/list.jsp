<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script type="text/javascript" src="${ctx }/resources/js/jquery-1.7.2.js"></script>
	<title>用户列表</title>
	<style type="text/css">
	body {
		font-size: 14px;
		color: #3c3c3c;
		text-align: center;
	}
	.list-table {
		border: solid 1px #000;
		border-collapse: collapse;
		width: 100%;
	}
	</style>
	<script type="text/javascript">
		$(function() {
			//ajax搜索
			$('#searchButton').click(function() {
				$.ajax({
					url : '${ctx}/user/searchAjax',
					type : 'POST',
					data : $('#searchForm').serialize(),
					dataType : 'json',
					success : function(data) {
						var list = data.list, content = [];
						for (var i = 0; i < list.length; i++) {
							content.push('<tr>');
							content.push('<td>' + (i + 1) + '</td>');
							content.push('<td>' + list[i].id + '</td>');
							content.push('<td>' + list[i].name + '</td>');
							content.push('<td>' + (list[i].age == null ? '' : list[i].age) + '</td>');
							content.push('<td>&nbsp;</td>');
							content.push('</tr>');
						}
						$('#dataTable').html(content.join(''));
					}
				});
			});
			
			if ('${msg}' != '') {
				alert('${msg}');
			}
		});
	</script>
</head>
<body>
	<div style="text-align: left; width: 1000px; margin: 0px auto; border: solid 1px #000; padding: 15px;">
	<h2>搜索</h2>
	<form id="searchForm" action="${ctx }/user/search" method="post">
		<table>
			<tr>
				<td>编号：</td>
				<td><input name="id" value="${params.id }" type="text" /></td>
				<td>姓名：</td>
				<td><input name="name" value="${params.name }" type="text" /></td>
				<td>
					<input type="hidden" name="pageNo" value="${params.pageNo }" />
					<input type="hidden" name="limit" value="${params.limit }" />
					<input type="submit" value="Form搜索" />
					<input type="button" value="Ajax搜索" id="searchButton" />
					<a href="${ctx }/user/add">新增用户</a>
				</td>
			</tr>
		</table>
	</form>
	<h2>用户列表</h2>
	<table class="list-table" border="1" cellpadding="4" cellspacing="0">
		<tr style="text-align: center;">
			<th>序号</th>
			<th>编号</th>
			<th>姓名</th>
			<th>年龄</th>
			<th>账号</th>
			<th>密码</th>
			<th>Salt</th>
			<th>管理</th>
		</tr>
		<tbody id="dataTable">
			<c:forEach items="${userList }" var="user" varStatus="status">
			<tr>
				<td>${status.index + 1 }</td>
				<td>${user.id }</td>
				<td>${user.name }</td>
				<td>${user.age }</td>
				<td>${user.loginName }</td>
				<td>${user.password }</td>
				<td>${user.salt }</td>
				<td>
					<a href="${ctx }/user/update/${user.id}">修改</a>
					<a href="${ctx }/user/delete/${user.id}" style="padding-left: 20px;">删除</a>
					<a href="${ctx }/user/delete?id=${user.id}" style="padding-left: 20px;">删除2</a>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
</body>
</html>