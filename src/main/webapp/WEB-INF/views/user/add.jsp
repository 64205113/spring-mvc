<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>添加用户</title>
	<style type="text/css">
		.error { color: red; }
	</style>
</head>
<body>
	<form action="${ctx }/user/${action }" method="post">
	<table>
		<tr>
			<td>姓名：</td>
			<td>
				<input type="text" name="name" value="${userInfo.name }" />
				<form:errors path="userInfo.name" cssClass="error"></form:errors>
			</td>
		</tr>
		<tr>
			<td>年龄：</td>
			<td>
				<input type="text" name="age" value="${userInfo.age }" />
				<form:errors path="userInfo.age" cssClass="error"></form:errors>
			</td>
		</tr>
		<tr>
			<td>账号：</td>
			<td>
				<input type="text" name="loginName" value="${userInfo.loginName }" />
				<form:errors path="userInfo.loginName" cssClass="error"></form:errors>
			</td>
		</tr>
		<c:if test="${action == 'add' }">
		<tr>
			<td>密码：</td>
			<td>
				<input type="password" name="password" value="${userInfo.password }" />
				<form:errors path="userInfo.password" cssClass="error"></form:errors>
			</td>
		</tr>
		</c:if>
		<tr>
			<td colspan="2">
				<input type="hidden" name="id" value="${userInfo.id }" />
				<input type="submit" value="提交" />
				${msg }
			</td>
		</tr>
	</table>
	</form>
</body>
</html>