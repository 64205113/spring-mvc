<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>spring mvc</title>
</head>
<body>
	<shiro:user>
		欢迎你，<shiro:principal />&nbsp;&nbsp;
		你的角色：
			<shiro:hasRole name="admin">admin</shiro:hasRole>&nbsp;
			<shiro:hasRole name="user">user</shiro:hasRole>
		<a href="${ctx }/logout">退出登录</a>
	</shiro:user><br /><br />
	<a href="${ctx }/user/search">用户管理</a><br /><br />
	<a href="${ctx }/util/upload">文件上传与下载</a><br /><br />
</body>
</html>