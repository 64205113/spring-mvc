<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>用户登录</title>
	<style type="text/css">
		.error { color: red; }
	</style>
	<script type="text/javascript" src="${ctx }/resources/js/jquery-1.7.2.js"></script>
	<script type="text/javascript">
	$(function() {
		if ('${msg}' != '') {
			alert('${msg}');
		}
		
		$('#changeCaptcha').click(function() {
			$('#imgCaptcha').attr('src', '${ctx }/captcha-image?' + Math.random());
		});
	});
	
	function changeLocale(locale) {
		window.location.href = '${ctx}/changeLocale/' + locale;
	}
	</script>
</head>
<body>
<form action="${ctx }/login" method="post">
	账号：<input type="text" name="username" value="tongyufu" />
	<form:errors path="loginInfo.username" cssClass="error" /><br />
	密码：<input type="password" name="password" value="admin" />
	<form:errors path="loginInfo.password" cssClass="error" /><br />
	验证码：<input type="text" name="safeCode" value="${safeCode }" />
	<img id="imgCaptcha" src="${ctx }/captcha-image" alt="" width="70" height="27" />
	<a href="javascript:void(0);" id="changeCaptcha">看不清？点击更换</a><br />
	<label><input type="checkbox" name="rememberMe" checked="checked" />记住我</label><br />
	<input type="submit" value="登录" />
	<br /><s:message code="user.name"></s:message><br />
	<input type="button" value="中文" onclick="changeLocale('zh_CN')" />
	<input type="button" value="英文" onclick="changeLocale('en_US')" />
</form>
</body>
</html>