<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>文件上传</title>
	<script type="text/javascript" src="${ctx }/resources/js/jquery-1.7.2.js"></script>
	<script type="text/javascript" src="${ctx }/resources/js/jquery.form-3.24.js"></script>
	<script type="text/javascript">
		$(function() {
			if ('${msg}' != '') {
				alert('${msg}');
			}
		});
	</script>
</head>
<body>
	<h2>单文件上传</h2>
	<form action="${ctx }/util/singleUpload" method="post" enctype="multipart/form-data">
		<input type="file" name="photo" />
		<input type="submit" value="上传" />
	</form>
	<h2>多文件上传</h2>
	<form action="${ctx }/util/multiUpload" method="post" enctype="multipart/form-data">
		<input type="file" name="photos" />
		<input type="file" name="photos" />
		<input type="submit" value="上传" />
	</form>
	
	<h2>文件下载</h2>
	<a href="${ctx }/util/download">点我下载文件</a><br /><br />
	<a href="${ctx }/util/exportExcel">导出Excel数据</a>
</body>
</html>