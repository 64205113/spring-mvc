package com.online.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

public class UserInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	@NotBlank(message = "姓名不可以为空")
	private String name;
	@Min(value = 18, message = "年龄不能小于18岁")
	private Integer age;
	@NotBlank(message = "账号不可以为空")
	private String loginName;
	@NotBlank(message = "密码不可以为空")
	private String password;
	private String salt;
	private List<BlogInfo> blogs = new ArrayList<BlogInfo>();

	public UserInfo() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public List<BlogInfo> getBlogs() {
		return blogs;
	}

	public void setBlogs(List<BlogInfo> blogs) {
		this.blogs = blogs;
	}

}
