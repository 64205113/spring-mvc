package com.online.model.user;

import java.io.Serializable;

/**
 * 用户博客
 * 
 * @author trunks
 *
 */
public class BlogInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private int uid;
	private String title;
	private String content;

	public BlogInfo() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
