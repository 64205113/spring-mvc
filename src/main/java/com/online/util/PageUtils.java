package com.online.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;

public class PageUtils {

	public static void processPageParams(Map<String, Object> params) {
		Integer pageNo = 1;
		Integer limit = 25;
		
		if (params == null) {
			params = new HashMap<String, Object>();
		}
		if (params.get("pageNo") == null) {
			params.put("pageNo", pageNo);
		} else if (NumberUtils.isDigits(params.get("pageNo").toString())) {
			pageNo = Integer.parseInt(params.get("pageNo").toString());
		}
		if (params.get("limit") == null) {
			params.put("limit", limit);
		} else if (NumberUtils.isDigits(params.get("limit").toString())) {
			limit = Integer.parseInt(params.get("limit").toString());
		}
		
		//计算后
		params.put("start", (pageNo - 1) * limit);
		params.put("end", pageNo * limit);
	}

}
