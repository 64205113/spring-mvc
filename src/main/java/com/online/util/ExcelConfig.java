package com.online.util;

import java.text.DateFormat;
import java.util.List;

/**
 * ExcelWrite导出配置(ExcelWriteUtil)
 * 
 * @author trunks
 * 
 */
public class ExcelConfig {

	private String sourceTemplate; // 模板文件路径
	private String targetFile; // 生成的文件路径
	private Integer pageSize; // 每页行数
	private DateFormat dateFormat; // 日期格式化
	private List<?> data; // 写入excel的数据
	private String[] autoSizeColumn; // 自适应列(属性名集合)
	private Boolean autoSize = false; // 自适应所有列
	private String[] sumColumn; // 汇总列(属性名集合)
	private Integer flushSize = 500; // 每写入指定行刷新一次，默认500。如果依然内存溢出，可考虑缩小此值。

	public ExcelConfig() {
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public DateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getSourceTemplate() {
		return sourceTemplate;
	}

	public void setSourceTemplate(String sourceTemplate) {
		this.sourceTemplate = sourceTemplate;
	}

	public String getTargetFile() {
		return targetFile;
	}

	public void setTargetFile(String targetFile) {
		this.targetFile = targetFile;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public String[] getAutoSizeColumn() {
		return autoSizeColumn;
	}

	public void setAutoSizeColumn(String[] autoSizeColumn) {
		this.autoSizeColumn = autoSizeColumn;
	}

	public String[] getSumColumn() {
		return sumColumn;
	}

	public void setSumColumn(String[] sumColumn) {
		this.sumColumn = sumColumn;
	}

	public Integer getFlushSize() {
		return flushSize;
	}

	public void setFlushSize(Integer flushSize) {
		this.flushSize = flushSize;
	}

	public Boolean getAutoSize() {
		return autoSize;
	}

	public void setAutoSize(Boolean autoSize) {
		this.autoSize = autoSize;
	}

}
