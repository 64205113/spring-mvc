package com.online.seri;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.io.FileUtils;

public class TestOut {

	static String FILE_NAME = "D:/temp.out";

	public static void main(String[] args) throws Exception {
		TestOut test = new TestOut();
		//test.write();
		test.read();
	}

	void write() throws IOException {
		FileOutputStream fs = new FileOutputStream(FILE_NAME);
		ObjectOutputStream out = new ObjectOutputStream(fs);
		out.writeObject(new UserInfo(1, "仝玉甫"));
		out.close();
	}

	void read() throws IOException, ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILE_NAME));
		UserInfo user = (UserInfo) in.readObject();
		System.out.println(user.getName());
		in.close();
	}
}
