package com.online.action;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

@Controller
public class HomeControl {

	@Autowired
	private CookieLocaleResolver localeResolver;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model, HttpServletResponse response) {
		Subject currentUser = SecurityUtils.getSubject();
		// System.out.println("===当前用户：" +
		// currentUser.getPrincipal().toString());
		return "home.jsp";
	}

	@RequestMapping(value = "/changeLocale/{locale}")
	public String changeLocale(@PathVariable String locale, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("========" + request.getLocale().toString());
		System.out.println(localeResolver.toString());
		localeResolver.setLocale(request, response, new Locale(locale));
		return "login.jsp";
	}
}
