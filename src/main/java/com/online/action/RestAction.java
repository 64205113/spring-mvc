package com.online.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Spring RestTemplate
 * 
 * @author trunks
 * 
 */
@Controller
@RequestMapping("/rest")
public class RestAction {

	@RequestMapping(value = "getForObjectAjax", method = RequestMethod.GET)
	@ResponseBody
	public String getForObjectAjax(@RequestParam String name) {
		return "Hello," + name;
	}

}
