package com.online.action.user;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.online.model.user.UserInfo;
import com.online.service.ServiceException;
import com.online.service.user.UserService;
import com.online.util.JsonUtils;
import com.online.util.PageUtils;

@Controller("userAction")
@RequestMapping("/user")
public class UserAction {

	@Autowired
	private UserService userService;

	/** 表单搜索 */
	@RequestMapping(value = "search")
	public String search(@RequestParam(required = false) HashMap<String, Object> params, Model model) {
		PageUtils.processPageParams(params);
		model.addAttribute("userList", userService.search(params));
		model.addAttribute("params", params);
		System.out.println(params);
		return "/user/list.jsp";
	}

	/** AJAX搜索 */
	@RequestMapping(value = "searchAjax", method = RequestMethod.POST)
	public @ResponseBody
	String searchAjax(@RequestParam Map<String, Object> params) {
		PageUtils.processPageParams(params);
		System.out.println(params);
		return JsonUtils.buildJSONSerializaer().writePageObjectJson(userService.search(params), params.get("total"));
	}

	/** 新增 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(Model model) {
		model.addAttribute("action", "add");
		return "/user/add.jsp";
	}

	/** 新增 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String add(@Valid @ModelAttribute UserInfo userInfo, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		String msg = "添加用户成功";
		if (result.hasErrors()) {
		    model.addAttribute("action", "add");
			model.addAttribute("msg", "添加用户失败");
			return "/user/add.jsp";
		}
		try {
			userService.add(userInfo);
		} catch (ServiceException e) {
			msg = e.getMessage();
		}
		redirectAttributes.addFlashAttribute("msg", msg);
		return "redirect:/user/search";
	}

	/** 修改 */
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("userInfo", userService.getById(id));
		model.addAttribute("action", "update");
		return "/user/add.jsp";
	}

	/** 修改 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@Valid @ModelAttribute("userInfo") UserInfo userInfo, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
		    model.addAttribute("action", "update");
			model.addAttribute("msg", "修改用户失败");
			return "/user/add.jsp";
		}
		userService.update(userInfo);
		redirectAttributes.addFlashAttribute("msg", "修改用户成功");
		return "redirect:/user/search";
	}

	/** 删除 */
	@RequestMapping("delete/{id}")
	public String delete(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
		String msg = "删除用户成功";
		try {
			userService.delete(id);
		} catch (ServiceException e) {
			msg = e.getMessage();
		}
		redirectAttributes.addFlashAttribute("msg", msg);
		return "redirect:/user/search";
	}

	/** 删除 */
	@RequestMapping("delete")
	public String delete2(@RequestParam("id") Integer id, RedirectAttributes redirectAttributes) {
		String msg = "删除用户成功";
		try {
			userService.delete2(id);
		} catch (Exception e) {
			msg = e.getMessage();
		}
		redirectAttributes.addFlashAttribute("msg", msg);
		return "redirect:/user/search";
	}

	/**
	 * 所有RequestMapping方法调用前的Model准备方法, 实现Struts2
	 * Preparable二次部分绑定的效果,先根据form的id从数据库查出Task对象,再把Form提交的内容绑定到该对象上。
	 * 因为仅update()方法的form中有id属性，因此仅在update时实际执行。<br />
	 * <b>删除2也会触发此方法</b>
	 */
	@ModelAttribute()
	public void getUser(@RequestParam(value = "id", required = false) Integer id, Model model) {
		if (id != null) {
			System.out.println("==========" + id);
			model.addAttribute("userInfo", userService.getById(id));
		}
	}
}
