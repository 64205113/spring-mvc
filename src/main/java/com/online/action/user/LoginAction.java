package com.online.action.user;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginAction {

	/** 没有权限时跳到此action */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login.jsp";
	}

	/** 登录验证失败时跳掉此action */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String fail(@RequestParam(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM) String userName, Model model) {
		model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, userName);
		model.addAttribute("msg", "登录失败");
		return "login.jsp";
	}

	/**
	 * 屏蔽 applicationContext-shiro.xml 中配置的logout，手动实现登出。
	 * 使用配置的logout时，遇到点击“登录”后再次登录，总是跳转到登录验证失败时的action，导致再次登录失败
	 */
	@RequestMapping(value = "/logout")
	public String logout() {
		SecurityUtils.getSubject().logout();
		return "login.jsp";
	}
}
