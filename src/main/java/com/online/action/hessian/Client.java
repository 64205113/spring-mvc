package com.online.action.hessian;

import java.net.MalformedURLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.caucho.hessian.client.HessianProxyFactory;

@Service
public class Client {

	@Resource(name = "accountService")
	private IAccount account;
	
	public void getAccount(double money) {
		System.out.println(account.getMoney(money));
	}
	
	public static void main(String[] args) {
		String url = "http://localhost:8080/spring-mvc/account";
		HessianProxyFactory factory = new HessianProxyFactory();
		try {
			IAccount account = (IAccount) factory.create(IAccount.class, url);
			System.out.println(account.getMoney(100d));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

}
