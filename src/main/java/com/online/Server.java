package com.online;

import java.io.File;
import java.net.URL;
import java.security.ProtectionDomain;

import org.eclipse.jetty.webapp.WebAppContext;

public class Server {

	public static void main(String[] args) {
		String contextPath = "/spring-mvc";
		int port = Integer.getInteger("port", 8080);
		org.eclipse.jetty.server.Server server = createServer(contextPath, port);
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(100);
		}
	}

	private static org.eclipse.jetty.server.Server createServer(String contextPath, int port) {
		System.setProperty("org.apache.jasper.compiler.disablejsr199", "true");
		org.eclipse.jetty.server.Server server = new org.eclipse.jetty.server.Server(port);
		server.setStopAtShutdown(true);

		ProtectionDomain protectionDomain = Server.class.getProtectionDomain();
		URL location = protectionDomain.getCodeSource().getLocation();
		String warFile = location.toExternalForm();
		WebAppContext context = new WebAppContext(warFile, contextPath);
		context.setServer(server);

		// 设置work dir,war包将解压到该目录，jsp编译后的文件也将放入其中。
		File workDir = new File(new File(location.getPath()).getParent(), "work");
		context.setTempDirectory(workDir);
		server.setHandler(context);

		return server;
	}

}
