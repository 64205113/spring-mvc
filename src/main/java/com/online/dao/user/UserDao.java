package com.online.dao.user;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.online.model.user.RoleInfo;
import com.online.model.user.UserInfo;

@Repository
public interface UserDao {

	List<UserInfo> search(Map<String, Object> params);

	Long searchTotal(Map<String, Object> params);

	UserInfo getById(Integer id);

	UserInfo getByLoginName(String loginName);

	/** 查询指定用户角色 */
	List<RoleInfo> getRoleByUserLoginName(String loginName);

	void add(UserInfo user);

	void update(UserInfo user);

	void delete(Integer id);

	void addUserRole(@Param("user_id") Integer user_id, @Param("role_id") Integer role_id);
}
