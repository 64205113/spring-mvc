package com.online.dao.user;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.online.model.user.BlogInfo;

@Repository
public interface BlogDao {

	List<BlogInfo> queryByUserId(Integer uid);
	
}
