package com.online;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.client.RestTemplate;

/**
 * Spring RestTemplate
 * 
 * @author trunks
 * 
 */
public class Restful {

	private RestTemplate template = new RestTemplate();
	static final String HOST = "http://localhost:8080/spring-mvc/rest/";
	
	public static void main(String[] args) {
		Restful restful = new Restful();
		restful.getForObjectAjax();
	}

	/**请求网址*/
	void getForObject() {
		String result = template.getForObject("http://www.baidu.com/", String.class);
		System.out.println(result);
	}
	
	/**Ajax*/
	void getForObjectAjax() {
		String url = HOST + "getForObjectAjax";
		Map<String, String> params = Collections.singletonMap("name", "仝玉甫");
		Map<String, String> uriVariables = new HashMap<String, String>();
		uriVariables.put("name", "仝玉甫");
		String result = template.getForObject(url, String.class, uriVariables);
		System.out.println(result);
	}
	
}
