package com.online.showcase;

import java.util.ArrayList;
import java.util.List;

public class Memory {

	public static final int MB = 1024 * 1024;
	public static final Integer KB = 1024;

	public static void main(String[] args) {

		List<byte[]> list = new ArrayList<byte[]>();
		for (int i = 0; i < 500; i++) {
			byte[] bytes = new byte[MB];
			list.add(bytes);
		}
		list.clear();
		System.gc();
		Runtime runtime = Runtime.getRuntime();
		System.out.println(runtime.freeMemory() / MB);
		System.out.println(runtime.totalMemory() / MB);
		System.out.println(runtime.maxMemory() / MB);
		while(true) {
			
		}
	}

}
